package com.avanty.socket;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import java.util.concurrent.ConcurrentHashMap;

public class WebSocketHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    @Override
    public void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) {
        Channel channel = ctx.channel();
        System.out.println(channel.remoteAddress() + " > read from web : " + msg.text());

        ChannelMap channelMap = ChannelMap.getInstance();
        channelMap.putWebSockCtx(channel.id().asLongText(), ctx);

        ConcurrentHashMap<String, ChannelHandlerContext> terminalCtxs = channelMap.getTerminalCtxs();
        for (ChannelHandlerContext terminalCtx : terminalCtxs.values()) {
            terminalCtx.channel().writeAndFlush(msg.text());
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.flush();
        ctx.close();
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        System.out.println("用户下线: " + ctx.channel().id().asLongText());
    }
}
