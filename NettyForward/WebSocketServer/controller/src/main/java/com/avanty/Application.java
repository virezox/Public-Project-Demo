package com.avanty;

import com.avanty.socket.EchoServer;
import com.avanty.socket.WebSocketServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.*")
public class Application {

	public static void main(String[] args) throws Exception {
		new EchoServer().service();
		new WebSocketServer().service();
		SpringApplication.run(Application.class, args);
	}
}
