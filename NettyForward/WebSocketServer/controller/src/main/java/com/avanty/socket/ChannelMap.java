package com.avanty.socket;

import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.ConcurrentHashMap;

public class ChannelMap {
    private static ChannelMap instance;
    private ConcurrentHashMap<String,ChannelHandlerContext> TerminalMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String,ChannelHandlerContext> WebSockMap = new ConcurrentHashMap<>();

    private ChannelMap() { }

    public static synchronized ChannelMap getInstance() {
        if (instance == null) {
            instance = new ChannelMap();
        }
        return instance;
    }

    /**
     * Terminal
     */
    public void putTerminalCtx(String id, ChannelHandlerContext ctx){
        if(!TerminalMap.containsKey(id)){
            TerminalMap.put(id,ctx);
        }
    }

    public ChannelHandlerContext getTerminalCtx(String id){
        return TerminalMap.get(id);
    }

    public ConcurrentHashMap<String,ChannelHandlerContext> getTerminalCtxs(){
        return TerminalMap;
    }

    /**
     * WebSocket
     */
    public void putWebSockCtx(String id, ChannelHandlerContext ctx){
        if(!WebSockMap.containsKey(id)){
            WebSockMap.put(id,ctx);
        }
    }

    public ChannelHandlerContext getWebSockCtx(String id){
        return WebSockMap.get(id);
    }

    public ConcurrentHashMap<String,ChannelHandlerContext> getWebSockCtxs(){
        return WebSockMap;
    }

}
