package com.avanty.socket;

import io.netty.channel.*;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

public class EchoServerChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel){
        ChannelPipeline pipeline = socketChannel.pipeline();

        /**
         LengthFieldBasedFrameDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength,
         int lengthAdjustment, int initialBytesToStrip)
         定义最大帧的长度，
         起始指针(偏移量)，
         结束偏移量(起始偏移量+长度)，
         长度调节值(当总长包含头部信息的时候，这个可以是个负数)，
         解码后的数据包需要跳过的头部信息的字节数，
         */

        //pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 0, 0, 0));
        /**
         new LengthFieldPrepender(4, false);初始化编码器
         第2个参数若为true时，则编码时会将数据包的Length字段字节数叠加的到Length值中。
         */

        //pipeline.addLast(new LengthFieldPrepender(0));
        /**
         String mytext = java.net.URLEncoder.encode("中国", "utf-8");
         String mytext2 = java.net.URLDecoder.decode(mytext, "utf-8");
         这两条语句在同一个页面中的话,得到的结果是:
         mytext: %E4%B8%AD%E5%9B%BD
         mytex2: 中国
         */
        pipeline.addLast(new StringDecoder(CharsetUtil.UTF_8));
        pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));

        pipeline.addLast(new EchoServerHandler());
    }
}
