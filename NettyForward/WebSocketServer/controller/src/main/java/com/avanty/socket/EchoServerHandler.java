package com.avanty.socket;

import com.alibaba.fastjson.JSONObject;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import java.util.concurrent.ConcurrentHashMap;

public class EchoServerHandler extends ChannelInboundHandlerAdapter{
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        Channel channel = ctx.channel();
        System.out.println(channel.remoteAddress() + " > read from Apps : " + msg.toString());
        JSONObject obj = JSONObject.parseObject(msg.toString());
        String id = obj.getString("id");
        ChannelMap channelMap = ChannelMap.getInstance();
        channelMap.putTerminalCtx(id,ctx);

        /*
            {
                "id": "JDJK - JFLS - IRPE - 8505",
                "msg": "nihao"
            }
        */

        String message = obj.getString("msg");
        ConcurrentHashMap<String,ChannelHandlerContext> targetCtxs = channelMap.getWebSockCtxs();
        for(ChannelHandlerContext targetCtx : targetCtxs.values()){
            targetCtx.channel().writeAndFlush(new TextWebSocketFrame(message));
        }

    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.flush();
        ctx.close();
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        System.out.println("用户下线: " + ctx.channel().id().asLongText());
    }

}
