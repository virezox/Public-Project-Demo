package com.avanty.socket;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class WebSocketServer {
    /**
     * 用于分配处理业务线程的线程组个数
     */
    private final int BIZGROUPSIZE = Runtime.getRuntime().availableProcessors() * 2;        //默认
    /**
     * 业务出现线程大小
     */
    private final int BIZTHREADSIZE = 2;
    //BOSS线程池，用于接受请求的线程组
    private final EventLoopGroup bossGroup = new NioEventLoopGroup(BIZGROUPSIZE);
    //WORK线程池，用于处理IO操作线程组
    //参照ThreadFactory threadFactory = new DefaultThreadFactory("work thread pool");
    private final EventLoopGroup workerGroup = new NioEventLoopGroup(BIZTHREADSIZE);

    public void service() throws Exception {
        try {
            //服务启动器
            ServerBootstrap bootstrap = new ServerBootstrap();
            //指定Netty的Boss线程和work线程，bossGroup收到请求之后叫workerGroup去处理
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 100)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new WebSocketChannelInitializer());
            /*
                ChannelFuture f = bootstrap.bind(IP, PORT).sync();		//线程同步阻塞等待服务器绑定到指定端口
                ChannelFuture f = bootstrap.bind(IP, PORT);
            */

            ChannelFuture f = bootstrap.bind(30066);
//            f.channel().closeFuture();       //成功绑定到端口之后,给channel增加一个 管道关闭的监听器并同步阻塞,直到channel关闭,线程才会往下执行

        } finally {
            // Shut down all event loops to terminate all threads.
//            bossGroup.shutdownGracefully();
//            workerGroup.shutdownGracefully();
        }
    }

    protected void shutdown() {
        workerGroup.shutdownGracefully();
        bossGroup.shutdownGracefully();
    }
}