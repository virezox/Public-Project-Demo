package com.avanty.activiti;

import org.activiti.engine.*;
import org.activiti.engine.impl.pvm.process.ProcessDefinitionImpl;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActivitiConfig {

	/**
	 * 配合xml配置文件使用
	 */
	@Bean
	public ProcessEngine processEngine() {
		String resource = "activiti/activitiConfig.xml";
		String beanName = "processEngineConfiguration";
		ProcessEngineConfiguration conf = ProcessEngineConfiguration
				.createProcessEngineConfigurationFromResource(resource, beanName);
		return conf.buildProcessEngine();
	}

	/**
	 * @ Bean可以用xml配置，也可以代码配置
	 * @Bean public ProcessEngine processEngine() { //创建Activiti配置对象实例
	 *       ProcessEngineConfiguration configuration =
	 *       ProcessEngineConfiguration.createStandaloneInMemProcessEngineConfiguration();
	 *       configuration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
	 *       configuration.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/activiti?characterEncoding=utf8&useSSL=false&nullCatalogMeansCurrent=true");
	 *       configuration.setJdbcUsername("root");
	 *       configuration.setJdbcPassword("MySQLadmin1!"); //设置建表策略
	 *       configuration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
	 *       //得到引擎实例 return configuration.buildProcessEngine(); }
	 */

	@Bean
	public RepositoryService repositoryService(ProcessEngine processEngine) {
		return processEngine.getRepositoryService();
	}

	@Bean
	public RuntimeService runtimeService(ProcessEngine processEngine) {
		return processEngine.getRuntimeService();
	}

	@Bean
	public TaskService taskService(ProcessEngine processEngine) {
		return processEngine.getTaskService();
	}

	@Bean
	public HistoryService historyService(ProcessEngine processEngine) {
		return processEngine.getHistoryService();
	}

	@Bean
	public ManagementService managementService(ProcessEngine processEngine) {
		return processEngine.getManagementService();
	}

	@Bean
	public IdentityService identityService(ProcessEngine processEngine) {
		return processEngine.getIdentityService();
	}

}
