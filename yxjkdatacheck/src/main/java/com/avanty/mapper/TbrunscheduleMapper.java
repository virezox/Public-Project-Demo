package com.avanty.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: Michael Zhan
 * @description: Tbrunschedule
 * @create: 2019-10-03 11:21:39
 */

@Mapper
public interface TbrunscheduleMapper {
    List selectScheduleDuration(@Param("startTime") String startTime, @Param("endTime") String endTime);
}
