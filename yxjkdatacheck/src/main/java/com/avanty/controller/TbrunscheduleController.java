package com.avanty.controller;

import com.avanty.pojo.Tbrunschedule;
import com.avanty.service.impl.TbrunscheduleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author: Michael Zhan
 * @description: tbrunschedule
 * @create: 2019-10-03 11:28:57
 */

@RestController
public class TbrunscheduleController {

    @Autowired
    TbrunscheduleServiceImpl tbrunscheduleServiceImpl;

    @Autowired
    MongoTemplate mongoTemplate;

    @RequestMapping("/schedule/{startTime}/{endTime}")
    public List tbRunSchedule(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime) {
        return tbrunscheduleServiceImpl.selectScheduleDuration(startTime, endTime);
    }

    @RequestMapping("/mongo/{startTime}/{endTime}")
    public List mongoSchedule(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime) {
        Query query = new Query();
        Criteria sql = Criteria.where("updateTime").gte(startTime)
                .andOperator(Criteria.where("updateTime").lte(endTime));
        query.addCriteria(sql);
        return mongoTemplate.find(query, Tbrunschedule.class,"Tbrunschedule");
    }

    //db.tbrunschedule.find({"$and":[{"updateTime":{"$gte":"2019-10-01 00:00:00"}},{"updateTime":{"$lte":"2019-10-24 00:00:00"}}]})

    @RequestMapping("/check/{startTime}/{endTime}")
    public Map checkSchedule(@PathVariable("startTime") String startTime, @PathVariable("endTime") String endTime) {
        List<Tbrunschedule> mysqlSchedule = tbrunscheduleServiceImpl.selectScheduleDuration(startTime, endTime);
        Query query = new Query();
        Criteria sql = Criteria.where("updateTime").gte(startTime)
                .andOperator(Criteria.where("updateTime").lte(endTime));
        query.addCriteria(sql);
        List<Tbrunschedule> mongoSchedule = mongoTemplate.find(query, Tbrunschedule.class,"Tbrunschedule");
        List y = new ArrayList();
        List n = new ArrayList();
        if(mongoSchedule.size() > mysqlSchedule.size()){
            for(Tbrunschedule tbrunschedule : mongoSchedule){
                if(mysqlSchedule.contains(tbrunschedule)){
                    y.add(tbrunschedule);
                }else {
                    n.add(tbrunschedule);
                }
            }
        }
        Map res = new HashMap();
        res.put("y",y);
        res.put("n",n);
        return res;
    }
}
