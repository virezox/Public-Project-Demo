package com.avanty.service;

import java.util.List;

/**
 * @author: Michael Zhan
 * @description: Tbrunschedule
 * @create: 2019-10-03 11:24:42
 */

public interface TbrunscheduleService {
    List selectScheduleDuration(String startTime, String endTime);
}
