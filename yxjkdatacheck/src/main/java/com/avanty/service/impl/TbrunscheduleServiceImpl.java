package com.avanty.service.impl;

import com.avanty.mapper.TbrunscheduleMapper;
import com.avanty.service.TbrunscheduleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: Michael Zhan
 * @description: TbrunscheduleService
 * @create: 2019-10-03 11:25:44
 */
@Service
public class TbrunscheduleServiceImpl implements TbrunscheduleService {

    @Resource
    TbrunscheduleMapper tbrunscheduleMapper;

    @Override
    public List selectScheduleDuration(String startTime, String endTime) {
        return tbrunscheduleMapper.selectScheduleDuration(startTime, endTime);
    }
}
