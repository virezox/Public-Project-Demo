package com.avanty.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: Michael Zhan
 * @description: 运行图
 * @create: 2019-10-03 10:59:28
 */

@Data
public class Tbrunschedule implements Serializable {

    private String updateTime;
    private String stationCode;
    private Integer runId;
    private Integer txCode;
    private Integer antennaCode;
    private String antProg;
    private String orderType;
    private String startDate;
    private String endDate;
    private String startTime;
    private String endTime;
    private Integer freq;
    private String programCode;
    private String programName;
    private String azimuthM;
    private String azimuthDe;
    private Integer power;
    private Integer target;
    private String servArea;
    private String days;
    private String modCol;
    private String channel;
    private String runType;
    private String scoureType;
    private String insStCode;
    private String insTransCode;
    private Integer startId;
    private Integer stopId;
    private String rmks;
    private String msgNum;
    private String receiveTime;
    private String extend;

}
