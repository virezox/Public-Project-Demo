package com.avanty.yxjkdatacheck;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.avanty.*")
@MapperScan(basePackages = "com.avanty.mapper")
public class YxjkdatacheckApplication {

    public static void main(String[] args) {
        SpringApplication.run(YxjkdatacheckApplication.class, args);
    }

}
