$(function () {
    var icons = document.getElementsByTagName("img");
    var dock = document.getElementById("Dock");
    document.onmousemove = function (ev) {
        // || event 兼容IE event 名称
        var e = ev || event;

        for (var i = 0; i < icons.length; i++) {
            var x = icons[i].offsetLeft + icons[i].offsetWidth / 2;
            var y = icons[i].offsetTop + icons[i].offsetHeight / 2 + dock.offsetTop;
            var a = ev.clientX - x;
            var b = ev.clientY - y;
            var c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            var scale = 1 - c / 300;
            if (scale < 0.5) {
                scale = 0.5;
            }
            icons[i].style.width = scale * 140 + "px";
            icons[i].style.height = scale * 140 + "px";
        }
    };
});