package com.avanty.service;

import com.avanty.controller.ServiceError;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author zhan
 */
@Service
@FeignClient(value = "service-hello",fallback = ServiceError.class)
public interface HelloService {
    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    String sayHelloFromClient();
}
