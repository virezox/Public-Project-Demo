package com.avanty.controller;

import com.avanty.service.HelloService;
import org.springframework.stereotype.Component;

@Component
public class ServiceError implements HelloService{
    @Override
    public String sayHelloFromClient() {
        return "error error error!";
    }
}
