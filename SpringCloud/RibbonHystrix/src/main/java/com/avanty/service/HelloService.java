package com.avanty.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhan
 */
@Service
public class HelloService {
    @Autowired
    RestTemplate restTemplate;

    /**
     * @Hystrix 断路器指向hiError方法
     */
    @HystrixCommand(fallbackMethod = "hiError")
    public String helloService(){
        return restTemplate.getForObject("http://service-hello/hello",String.class);
    }

    public String hiError() {
        return "error error error!";
    }
}