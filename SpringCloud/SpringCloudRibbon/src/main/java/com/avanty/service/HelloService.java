package com.avanty.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author zhan
 */
@Service
public class HelloService {
    @Autowired
    RestTemplate restTemplate;
    public String helloService(){
        return restTemplate.getForObject("http://service-hello/hello",String.class);
    }
}
