package com.avanty.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhan
 */
@RestController
public class SayHello {
    @Value("${server.port}")
    String port;
    @RequestMapping("/hello")
    public String hello(){
        return "Hi," + "This is service on port:" + port + ".";
    }
}
