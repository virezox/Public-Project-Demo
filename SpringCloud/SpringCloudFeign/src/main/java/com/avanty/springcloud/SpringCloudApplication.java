package com.avanty.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zhan
 */
@SpringBootApplication
@EnableDiscoveryClient

/**
 * @author zhan
 * 扫描Feign接口
 */
@EnableFeignClients(basePackages = {"com.avanty.service"})
@ComponentScan(basePackages = {"com.avanty.controller"})
public class SpringCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudApplication.class, args);
	}
}
