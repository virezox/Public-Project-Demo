package com.avanty.controller;

import com.avanty.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhan
 */
@RestController
public class Hello {
    @Autowired
    HelloService helloService;
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String sayHello(){
        return helloService.sayHelloFromClient();
    }
}
