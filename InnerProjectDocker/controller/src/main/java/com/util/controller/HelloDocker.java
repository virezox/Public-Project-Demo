package com.util.controller;

import com.util.SayHi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloDocker {

    @RequestMapping("/inner_docker")
    public String helloDocker(){
        return new SayHi().sayHi();
    }
}
