package NettyEchoClient;

import com.alibaba.fastjson.JSONObject;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

import static java.lang.Thread.sleep;

public class EchoClient implements Runnable{
    private static final String IP = "127.0.0.1";
    private static final int PORT = 1234;
    public void run() {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group);
            b.channel(NioSocketChannel.class).option(ChannelOption.TCP_NODELAY, true);
            b.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                    ChannelPipeline pipeline = ch.pipeline();
                    /*
                         LengthFieldBasedFrameDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength,
                         int lengthAdjustment, int initialBytesToStrip)
                         定义最大帧的长度，
                         起始指针(偏移量)，
                         结束偏移量(起始偏移量+长度)，
                         长度调节值(当总长包含头部信息的时候，这个可以是个负数)，
                         解码后的数据包需要跳过的头部信息的字节数，
                     */
                    //pipeline.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                    //pipeline.addLast("frameEncoder", new LengthFieldPrepender(4));
                    pipeline.addLast(new StringDecoder(CharsetUtil.UTF_8));
                    pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));

                    pipeline.addLast(new EchoClientHandler());
                }
            });
            ChannelFuture f = b.connect(IP, PORT).sync();
            for(int i=0; i<10; i++) {
                JSONObject json = new JSONObject();
                json.put("id","1");
                json.put("toClient","2");
                json.put("sendValue","client 1 ： 第" + String.valueOf(i) + "次发送");
                f.channel().writeAndFlush(json.toJSONString());    //清空发送去
                sleep(1000);
            }
            //f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        new EchoClient().run();
    }

}
