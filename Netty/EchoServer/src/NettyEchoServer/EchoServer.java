package NettyEchoServer;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.CharsetUtil;

public class EchoServer {
    private static final String IP = "127.0.0.1";
    private static final int PORT = 1234;
    /**
     * 用于分配处理业务线程的线程组个数
     */
    private static final int BIZGROUPSIZE = Runtime.getRuntime().availableProcessors() * 2;        //默认
    /**
     * 业务出现线程大小
     */
    protected static final int BIZTHREADSIZE = 2;
    //BOSS线程池，用于接受请求的线程组
    private static final EventLoopGroup bossGroup = new NioEventLoopGroup(BIZGROUPSIZE);
    //WORK线程池，用于处理IO操作线程组
    //参照ThreadFactory threadFactory = new DefaultThreadFactory("work thread pool");
    private static final EventLoopGroup workerGroup = new NioEventLoopGroup(BIZTHREADSIZE);
    public static void service() throws Exception {
        try {
            //服务启动器
            ServerBootstrap bootstrap = new ServerBootstrap();
            //指定Netty的Boss线程和work线程，bossGroup收到请求之后叫workerGroup去处理
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 100)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            //ChannelPipeline通过ChannelHandlerContext间接管理ChannelHandler
                            ChannelPipeline pipeline = ch.pipeline();
                            /*
                               LengthFieldBasedFrameDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength,
                               int lengthAdjustment, int initialBytesToStrip)
                               定义最大帧的长度，
                               起始指针(偏移量)，
                               结束偏移量(起始偏移量+长度)，
                               长度调节值(当总长包含头部信息的时候，这个可以是个负数)，
                               解码后的数据包需要跳过的头部信息的字节数，
                            */
                            //pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 0, 0, 0));
                            /*
                                new LengthFieldPrepender(4, false);初始化编码器
                                第2个参数若为true时，则编码时会将数据包的Length字段字节数叠加的到Length值中。
                            */
                            //pipeline.addLast(new LengthFieldPrepender(0));
                            /*
                                String mytext = java.net.URLEncoder.encode("中国", "utf-8");
                                String mytext2 = java.net.URLDecoder.decode(mytext, "utf-8");
                                这两条语句在同一个页面中的话,得到的结果是:
                                mytext: %E4%B8%AD%E5%9B%BD
                                mytex2: 中国
                            */
                            pipeline.addLast(new StringDecoder(CharsetUtil.UTF_8));
                            pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));

                            pipeline.addLast(new EchoServerHandler());
                        }

                    });
            /*
                ChannelFuture f = bootstrap.bind(IP, PORT).sync();		//线程同步阻塞等待服务器绑定到指定端口
                ChannelFuture f = bootstrap.bind(IP, PORT);
            */
            ChannelFuture f = bootstrap.bind(PORT).sync();
            f.channel().closeFuture().sync();        //成功绑定到端口之后,给channel增加一个 管道关闭的监听器并同步阻塞,直到channel关闭,线程才会往下执行
            System.out.println("TCP服务器已启动");

        } finally {
            // Shut down all event loops to terminate all threads.
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    protected static void shutdown() {
        workerGroup.shutdownGracefully();
        bossGroup.shutdownGracefully();
    }

    public static void main(String[] args) throws Exception {
        System.out.println("开始启动TCP服务器...");
        EchoServer.service();
        //EchoServer.shutdown();
    }
}