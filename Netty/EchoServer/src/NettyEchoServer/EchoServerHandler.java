package NettyEchoServer;

import Model.ChannelMap;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EchoServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        System.out.println("read : " + msg);

        ChannelMap channelMap = ChannelMap.getInstance();
        JSONObject json = JSON.parseObject(msg.toString());

        String id = json.getString("id");

        channelMap.putChannelMap(id,ctx);

        String toClient = json.getString("toClient");
        if(toClient != null){
            ctx = (ChannelHandlerContext) channelMap.getChannelMap().get(toClient);
            if(ctx != null){
                ctx.write(json.getString("sendValue"));
            }
        }

    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
