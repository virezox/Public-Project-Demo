package Model;

import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.ConcurrentHashMap;

public class ChannelMap {
    private static ChannelMap instance;
    private ConcurrentHashMap<String,ChannelHandlerContext> concurrentHashMap = new ConcurrentHashMap<>();

    private ChannelMap() { }

    public static synchronized ChannelMap getInstance() {
        if (instance == null) {
            instance = new ChannelMap();
        }
        return instance;
    }

    public void putChannelMap(String id, ChannelHandlerContext ctx){
        if(!concurrentHashMap.containsKey(id)){
            concurrentHashMap.put(id,ctx);
        }
    }

    public ConcurrentHashMap getChannelMap(){
        return concurrentHashMap;
    }
}