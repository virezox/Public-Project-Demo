package com.avanty.controller;

import com.avanty.entity.Test1;
import com.avanty.service.master.Test1ServiceImpl;
import com.avanty.service.slave.Test2ServiceImpl;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class Hello {

    @Autowired
    Test1ServiceImpl test1Service;

    @Autowired
    Test2ServiceImpl test2Service;

    @RequestMapping("/hello1")
    @Transactional
    public String hello1() {

        Test1 test1 = new Test1();
        test1.setName("wa");
        test1.setUserAge(16);
        test1Service.insertTestOne(test1);

        return "SUCCESS";
    }

    @RequestMapping("/hello12")
    public Test1 hello12() {
        return test1Service.selectTestOne(1);
    }

    @RequestMapping("/hello123")
    public List<Test1> hello123() {
        PageHelper.startPage(1,3);
        return test1Service.selectTests();
    }

    @RequestMapping("/hello2")
    @Transactional
    public String hello2() {

        Map map1 = new HashMap();
        map1.put("name", "Zhan");
        map1.put("userAge", 18);
        test1Service.insertTest1(map1);

        Map map2 = new HashMap();
        map2.put("num", 6);
        map2.put("msg", "Hello");
        test2Service.insertTest2(map2);

        int i = 10 / 0;

        return "SUCCESS";
    }
}
