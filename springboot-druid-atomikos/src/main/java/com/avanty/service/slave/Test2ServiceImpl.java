package com.avanty.service.slave;

import com.avanty.mapper.slave.Test2Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class Test2ServiceImpl implements Test2Mapper {

    @Autowired
    Test2Mapper test2Mapper;

    @Override
    public void insertTest2(Map map) {
        test2Mapper.insertTest2(map);
    }

}
