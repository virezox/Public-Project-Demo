package com.avanty.service.master;

import com.avanty.entity.Test1;
import com.avanty.mapper.master.Test1Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class Test1ServiceImpl implements Test1Mapper {

    @Autowired
    Test1Mapper test1Mapper;

    @Override
    public void insertTestOne(Test1 test1) {
        test1Mapper.insertTestOne(test1);
    }

    @Override
    public Test1 selectTestOne(Integer id) {
        return test1Mapper.selectTestOne(id);
    }

    @Override
    public List<Test1> selectTests() {
        return test1Mapper.selectTests();
    }

    @Override
    public void insertTest1(Map map) {
        test1Mapper.insertTest1(map);
    }

}
