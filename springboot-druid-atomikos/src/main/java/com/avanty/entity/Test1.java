package com.avanty.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "Test1")
public class Test1 implements Serializable {

    //指定自增策略
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    //若没有开启驼峰命名，或者表中列名不符合驼峰规则，可通过该注解指定数据库表中的列名，exist标明数据表中有没有对应列
    @TableField(value = "name",exist = true)
    private String name;

    @TableField(value = "user_age",exist = true)
    private Integer userAge;

}
