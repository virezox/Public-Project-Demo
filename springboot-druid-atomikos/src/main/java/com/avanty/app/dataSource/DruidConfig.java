package com.avanty.app.dataSource;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.icatch.jta.UserTransactionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import java.sql.SQLException;

@Configuration
public class DruidConfig extends AbstractDataSourceConfig{

    @Bean(name = "master")
    @Primary
    public DataSource master(Environment env) throws SQLException {
        String prefix = "spring.datasource.druid.master.";
        return createAtomikosDataSourceBean(env, prefix, "master");
    }

    @Bean(name = "slave")
    public DataSource slave(Environment env) throws SQLException {
        String prefix = "spring.datasource.druid.slave.";
        return createAtomikosDataSourceBean(env, prefix, "slave");
    }


    @Bean(name = "xaTransactionManager")
    public JtaTransactionManager regTransactionManager () {
        UserTransactionManager userTransactionManager = new UserTransactionManager();
        UserTransaction userTransaction = new UserTransactionImp();
        return new JtaTransactionManager(userTransaction, userTransactionManager);
    }
}
