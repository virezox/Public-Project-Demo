package com.avanty.app.dataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = "com.avanty.mapper.slave", sqlSessionFactoryRef = "sqlSessionFactorySlave", sqlSessionTemplateRef = "sqlSessionTemplateSlave")
public class SlaveConfig extends AbstractDataSourceConfig {

    @Autowired
    @Qualifier("slave")
    private DataSource slave;

    @Bean(name = "sqlSessionFactorySlave")
    public SqlSessionFactory sqlSessionFactorySlave()
            throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(slave);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/slave/*Mapper.xml"));
        return sqlSessionFactoryBean.getObject();
    }


    @Bean(name = "sqlSessionTemplateSlave")
    public SqlSessionTemplate sqlSessionTemplateSlave(
            @Qualifier("sqlSessionFactorySlave") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
