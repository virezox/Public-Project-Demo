package com.avanty.mapper.master;

import com.avanty.entity.Test1;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface Test1Mapper {
    void insertTestOne(Test1 test1);
    Test1 selectTestOne(Integer id);
    List<Test1> selectTests();
    void insertTest1(Map map);
}
