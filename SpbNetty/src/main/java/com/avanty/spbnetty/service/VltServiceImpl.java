package com.avanty.spbnetty.service;

import com.avanty.spbnetty.dao.VltMapper;
import com.avanty.spbnetty.pojo.Vlt;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VltServiceImpl {

    @Resource
    private VltMapper vltMapper;

    public int addVlt(Vlt vlt){
       return vltMapper.add(vlt);
    }

    public List selectAll(){
        return vltMapper.selectAll();
    }

}
