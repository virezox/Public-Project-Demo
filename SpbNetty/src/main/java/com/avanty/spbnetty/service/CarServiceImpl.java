package com.avanty.spbnetty.service;

import com.avanty.spbnetty.dao.CarMapper;
import com.avanty.spbnetty.pojo.Car;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CarServiceImpl {

    @Resource
    private CarMapper carMapper;

    public int addCar(Car car){
       return carMapper.add(car);
    }

    public Car findByBrand(String brand){
        return carMapper.findByBrand(brand);
    }

    public List selectAll(){
        return carMapper.selectAll();
    }

}
