package com.avanty.spbnetty.dao;

import com.avanty.spbnetty.pojo.Car;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CarMapper extends BaseMapper{
    Car findByBrand(@Param("brand") String brand);
}
