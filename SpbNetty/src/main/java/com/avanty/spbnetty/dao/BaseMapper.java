package com.avanty.spbnetty.dao;

import java.util.List;

public interface BaseMapper {
    int add(Object obj);
    Object findById(Integer id);
    List selectAll();
    int insert(Object obj);
    int update(Object obj);
    int delete(Object obj);
}
