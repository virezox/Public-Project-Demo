package com.avanty.spbnetty.netty;

import com.avanty.Utils.BusinessThreadUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EchoHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) {

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        /**
         * @Test 测试线程池
         */
        BusinessThreadUtil.doBusiness(ctx, msg);


        /*
         ctx.channel().write("recv : " + msg.toString());
         ctx.channel().flush();
         Car car = new Car();
         car.setCarBrand("Mustang");
         car.setCarNum("浙A8396L");
         CarServiceImpl carServiceImpl = SpbNettyApplication.getBean(CarServiceImpl.class);
         carServiceImpl.addCar(car);
         */
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {

    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        System.out.println("offline " + ctx.channel().remoteAddress());
    }

}