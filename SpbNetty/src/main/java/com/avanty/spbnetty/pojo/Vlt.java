package com.avanty.spbnetty.pojo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Getter
@Setter
public class Vlt implements Serializable {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "mvalue")
    private String mvalue;

}
