package com.avanty.spbnetty.pojo;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Getter
@Setter
public class Car implements Serializable {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "car_band")
    private String carBrand;

    @Column(name = "car_num")
    private String carNum;

}
