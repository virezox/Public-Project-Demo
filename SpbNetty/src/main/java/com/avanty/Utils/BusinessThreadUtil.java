package com.avanty.Utils;

import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class BusinessThreadUtil {
    private static final ExecutorService executor = new ThreadPoolExecutor(10, 20, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(20));

    public static void doBusiness(ChannelHandlerContext ctx, Object msg) {
        //异步线程池处理
        executor.submit(() -> {
            int i = CountUtil.getInstance().returnCount();
            System.out.println(ctx.channel().remoteAddress() + "  " + msg.toString() + "  " + i + " ");
            ctx.channel().write("recv : " + msg.toString() + i);
            ctx.channel().flush();
        });
    }
}
