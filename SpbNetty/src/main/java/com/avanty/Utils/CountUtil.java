package com.avanty.Utils;

public class CountUtil {

    public int count = 1;
    private static class CountUtilHolder {
        public final static CountUtil instance = new CountUtil();
    }

    public static CountUtil getInstance() {
        return CountUtilHolder.instance;
    }

    public int returnCount(){
        return count++;
    }
}
