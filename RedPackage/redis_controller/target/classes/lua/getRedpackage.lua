local packet_count_id = KEYS[1]
local packet_amount_id = KEYS[2]
local user_id = KEYS[3]
local red_packet_id = KEYS[4]

redis.log(redis.LOG_DEBUG, packet_count_id)

local bloom_name = red_packet_id .. '_BLOOM_GRAB_REDPACKET'
local rcount = redis.call('GET', packet_count_id)

redis.log(redis.LOG_DEBUG, rcount)
redis.log(redis.LOG_DEBUG, tonumber(rcount))

local ramount = redis.call('GET', packet_amount_id)
local amount = ramount

if tonumber(rcount) > 0 then
    local flag = redis.call('BF.EXISTS', bloom_name, user_id)
    if (flag == 1) then
        return "1"
    elseif (tonumber(rcount) ~= 1) then
        local maxamount = ramount / rcount * 2
        amount = math.random(0.01, maxamount)
    end
    local result_2 = redis.call('DECR', packet_count_id)
    local result_3 = redis.call('DECRBY', packet_amount_id, amount)
    redis.call('BF.ADD', bloom_name, user_id)
    return amount .. "SPLIT" .. rcount
else
    return "0"
end
