package com.avanty.service;

import com.avanty.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
public class RedisServiceImpl {

    @Autowired
    RedisUtil redisUtil;

    public final String TOTAL_AMOUNT = "_TOTAL_AMOUNT";
    public final String TOTAL_COUNT = "_TOTAL_COUNT";

    @Transactional
    public Map distribute(String userId, int total_amount, int total_count) {
        Map redPacketInfo = new HashMap();
        redPacketInfo.put("red_packet_id", "rpg");  //UUID
        redPacketInfo.put("id", redPacketInfo.get("red_packet_id"));
        redPacketInfo.put("total_amount", total_amount);
        redPacketInfo.put("total_packet", total_count);
        redPacketInfo.put("user_id", userId);

        redisUtil.set(redPacketInfo.get("red_packet_id") + TOTAL_COUNT, total_count + "");
        redisUtil.set(redPacketInfo.get("red_packet_id") + TOTAL_AMOUNT, total_amount + "");

        return redPacketInfo;
    }

    /**
     * 抢红包
     */
    public Map getRedpackage(String userId, String redPacketId) {
        String msg = "很遗憾，红包已经被抢完";
        boolean resultFlag = false;
        double amountdb = 0.00;
        Map redPacketRecord = new HashMap();
        //  抢红包的过程必须保证原子性，此处可以加分布式锁
        if (Integer.parseInt(redisUtil.get(redPacketId + TOTAL_COUNT)) > 0) {    //  判断红包是否被抢完
            //  真正抢红包的过程，通过lua脚本处理保证原子性
            //  lua脚本逻辑中包含了计算抢红包金额
            //  若此处分开获取红包金额、剩余红包个数，则无法保证原子性
            String result = grubFromRedis(redPacketId + TOTAL_COUNT, redPacketId + TOTAL_AMOUNT, userId, redPacketId);
            if (!result.equals("0") && !result.equals("1")) {
                String resultAmount = result.split("SPLIT")[0];
                String resultRemaining = result.split("SPLIT")[1];
                int amount = Integer.parseInt(resultAmount);
                redPacketRecord.put("amount", amount);
                amountdb = amount / 100.00;
                msg = "恭喜你抢到红包，红包金额" + amountdb + "元！"
                        +
                        ",剩余红包：" + (Integer.parseInt(resultRemaining) - 1) + "个";
                resultFlag = true;
                //  记账
            } else if (result.equals("0")) {
                redPacketRecord.put("amount", 0);
                amountdb = 0.00;
                msg = "很遗憾，红包已经被抢完";
            } else if (result.equals("1")) {
                redPacketRecord.put("amount", 0);
                amountdb = 0.00;
                msg = "您已经抢过红包";
            } else {
                redPacketRecord.put("amount", 0);
                amountdb = 0.00;
                msg = "系统错误";
            }
        }
        Map res = new HashMap();
        res.put("msg", msg);
        res.put("resultFlag", resultFlag);
        res.put("amount", amountdb);
        res.put("red_packet_id", redPacketId);
        res.put("user_id", userId);
        return res;
    }

    /**
     * 抢红包原子操作，返回本次抢红包金额
     */
    public String grubFromRedis(String packet_count_id, String packet_amount_id, String userId, String redPacketId) {
        DefaultRedisScript<String> redisScript;
        redisScript = new DefaultRedisScript();
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("lua/getRedpackage.lua")));
        redisScript.setResultType(String.class);
        System.out.println(packet_count_id + " " + packet_amount_id + " " + userId + " " + redPacketId);
        return redisUtil.excuteLua(redisScript, packet_count_id, packet_amount_id, userId, redPacketId).toString();
    }
}

