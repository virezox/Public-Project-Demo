package com.avanty.controller;

import com.avanty.service.RedisServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/")
public class RedpackageController {

    @Autowired
    RedisServiceImpl service;

    /**
     * 发红包
     */
    @RequestMapping("/distribute/{user_id}/{total_count}/{totol_amount}")
    public Map distribute(@PathVariable("user_id") String user_id, @PathVariable("total_count") Integer total_count, @PathVariable("totol_amount") Double totol_amount) {
        return service.distribute(user_id, (int)(totol_amount * 100), total_count);
    }


    /**
     * 抢红包
     */
    @RequestMapping("/getRedpackage/{userId}/{redPacketId}")
    public Map getRedpackage(@PathVariable("userId") String userId, @PathVariable("redPacketId") String redPacketId) {
        return service.getRedpackage(userId, redPacketId);
    }

}
